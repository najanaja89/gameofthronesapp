﻿namespace GameOfThronesApp.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using System.Windows.Media.Imaging;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class GoTCharacters
    {
        [JsonProperty("titles")]
        public List<string> Titles { get; set; }

        [JsonProperty("spouse")]
        public List<string> Spouse { get; set; }

        [JsonProperty("children")]
        public List<string> Children { get; set; }

        [JsonProperty("allegiance")]
        public List<string> Allegiance { get; set; }

        [JsonProperty("books")]
        public List<Book> Books { get; set; }

        [JsonProperty("plod")]
        public long Plod { get; set; }

        [JsonProperty("longevity")]
        public List<object> Longevity { get; set; }

        [JsonProperty("plodB")]
        public double PlodB { get; set; }

        [JsonProperty("plodC")]
        public long PlodC { get; set; }

        [JsonProperty("longevityB")]
        public List<double> LongevityB { get; set; }

        [JsonProperty("longevityC")]
        public List<object> LongevityC { get; set; }

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("gender")]
        public Gender? Gender { get; set; }

        [JsonProperty("culture", NullValueHandling = NullValueHandling.Ignore)]
        public string Culture { get; set; }

        [JsonProperty("house", NullValueHandling = NullValueHandling.Ignore)]
        public string House { get; set; }

        [JsonProperty("alive")]
        public bool Alive { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("__v")]
        public long V { get; set; }

        [JsonProperty("pagerank")]
        public Pagerank Pagerank { get; set; }

        [JsonProperty("id")]
        public string GoTCharacterId { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Image { get; set; }

        [JsonProperty("birth", NullValueHandling = NullValueHandling.Ignore)]
        public long? Birth { get; set; }

        [JsonProperty("placeOfDeath", NullValueHandling = NullValueHandling.Ignore)]
        public string PlaceOfDeath { get; set; }

        [JsonProperty("death", NullValueHandling = NullValueHandling.Ignore)]
        public long? Death { get; set; }

        [JsonProperty("placeOfBirth", NullValueHandling = NullValueHandling.Ignore)]
        public string PlaceOfBirth { get; set; }

        [JsonProperty("longevityStartB", NullValueHandling = NullValueHandling.Ignore)]
        public long? LongevityStartB { get; set; }

        [JsonProperty("father", NullValueHandling = NullValueHandling.Ignore)]
        public string Father { get; set; }

        [JsonProperty("mother", NullValueHandling = NullValueHandling.Ignore)]
        public string Mother { get; set; }

        [JsonProperty("heir", NullValueHandling = NullValueHandling.Ignore)]
        public string Heir { get; set; }

        public override string ToString()
        {
            return $"{Name}";
        }
    }

    public partial class Pagerank
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }
    }

    public enum Book { AClashOfKings, ADanceWithDragons, ADanceWithDragonsTheWindsOfWinter, AFeastForCrows, AGameOfThrones, AStormOfSwords, AStormOfSwordsAppendix, AStormOfSwordsMentioned, BookAFeastForCrows, BookThePrincessAndTheQueen, BookTheWorldOfIceFire, Empty, FireAndBlood, FireBlood, GameOfThrones, TheHedgeKnight, TheMysteryKnight, ThePrincessAndTheQueen, TheRoguePrince, TheSonsOfTheDragon, TheSwornSword, TheWindsOfWinter, TheWorldOfIceFire };

    public enum Gender { Female, Male };

    public partial class GoTCharacters
    {
        public static List<GoTCharacters> FromJson(string json) => JsonConvert.DeserializeObject<List<GoTCharacters>>(json, GameOfThronesApp.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<GoTCharacters> self) => JsonConvert.SerializeObject(self, GameOfThronesApp.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                BookConverter.Singleton,
                GenderConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class BookConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Book) || t == typeof(Book?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "":
                    return Book.Empty;
                case "'The World of Ice & Fire":
                    return Book.BookTheWorldOfIceFire;
                case "A Clash of Kings":
                    return Book.AClashOfKings;
                case "A Dance with Dragons":
                    return Book.ADanceWithDragons;
                case "A Dance with Dragons  The Winds of Winter":
                    return Book.ADanceWithDragonsTheWindsOfWinter;
                case "A Feast For Crows":
                    return Book.BookAFeastForCrows;
                case "A Feast for Crows":
                    return Book.AFeastForCrows;
                case "A Game of Thrones":
                    return Book.AGameOfThrones;
                case "A Storm of Swords":
                    return Book.AStormOfSwords;
                case "A Storm of Swords (appendix}":
                    return Book.AStormOfSwordsAppendix;
                case "A Storm of Swords (mentioned":
                    return Book.AStormOfSwordsMentioned;
                case "Fire & Blood":
                    return Book.FireBlood;
                case "Fire and Blood":
                    return Book.FireAndBlood;
                case "Game of Thrones":
                    return Book.GameOfThrones;
                case "The Hedge Knight":
                    return Book.TheHedgeKnight;
                case "The Mystery Knight":
                    return Book.TheMysteryKnight;
                case "The Princess and The Queen":
                    return Book.BookThePrincessAndTheQueen;
                case "The Princess and the Queen":
                    return Book.ThePrincessAndTheQueen;
                case "The Rogue Prince":
                    return Book.TheRoguePrince;
                case "The Sons of the Dragon":
                    return Book.TheSonsOfTheDragon;
                case "The Sworn Sword":
                    return Book.TheSwornSword;
                case "The Winds of Winter":
                    return Book.TheWindsOfWinter;
                case "The World of Ice & Fire":
                    return Book.TheWorldOfIceFire;
            }
            throw new Exception("Cannot unmarshal type Book");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Book)untypedValue;
            switch (value)
            {
                case Book.Empty:
                    serializer.Serialize(writer, "");
                    return;
                case Book.BookTheWorldOfIceFire:
                    serializer.Serialize(writer, "'The World of Ice & Fire");
                    return;
                case Book.AClashOfKings:
                    serializer.Serialize(writer, "A Clash of Kings");
                    return;
                case Book.ADanceWithDragons:
                    serializer.Serialize(writer, "A Dance with Dragons");
                    return;
                case Book.ADanceWithDragonsTheWindsOfWinter:
                    serializer.Serialize(writer, "A Dance with Dragons  The Winds of Winter");
                    return;
                case Book.BookAFeastForCrows:
                    serializer.Serialize(writer, "A Feast For Crows");
                    return;
                case Book.AFeastForCrows:
                    serializer.Serialize(writer, "A Feast for Crows");
                    return;
                case Book.AGameOfThrones:
                    serializer.Serialize(writer, "A Game of Thrones");
                    return;
                case Book.AStormOfSwords:
                    serializer.Serialize(writer, "A Storm of Swords");
                    return;
                case Book.AStormOfSwordsAppendix:
                    serializer.Serialize(writer, "A Storm of Swords (appendix}");
                    return;
                case Book.AStormOfSwordsMentioned:
                    serializer.Serialize(writer, "A Storm of Swords (mentioned");
                    return;
                case Book.FireBlood:
                    serializer.Serialize(writer, "Fire & Blood");
                    return;
                case Book.FireAndBlood:
                    serializer.Serialize(writer, "Fire and Blood");
                    return;
                case Book.GameOfThrones:
                    serializer.Serialize(writer, "Game of Thrones");
                    return;
                case Book.TheHedgeKnight:
                    serializer.Serialize(writer, "The Hedge Knight");
                    return;
                case Book.TheMysteryKnight:
                    serializer.Serialize(writer, "The Mystery Knight");
                    return;
                case Book.BookThePrincessAndTheQueen:
                    serializer.Serialize(writer, "The Princess and The Queen");
                    return;
                case Book.ThePrincessAndTheQueen:
                    serializer.Serialize(writer, "The Princess and the Queen");
                    return;
                case Book.TheRoguePrince:
                    serializer.Serialize(writer, "The Rogue Prince");
                    return;
                case Book.TheSonsOfTheDragon:
                    serializer.Serialize(writer, "The Sons of the Dragon");
                    return;
                case Book.TheSwornSword:
                    serializer.Serialize(writer, "The Sworn Sword");
                    return;
                case Book.TheWindsOfWinter:
                    serializer.Serialize(writer, "The Winds of Winter");
                    return;
                case Book.TheWorldOfIceFire:
                    serializer.Serialize(writer, "The World of Ice & Fire");
                    return;
            }
            throw new Exception("Cannot marshal type Book");
        }

        public static readonly BookConverter Singleton = new BookConverter();
    }

    internal class GenderConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Gender) || t == typeof(Gender?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "female":
                    return Gender.Female;
                case "male":
                    return Gender.Male;
            }
            throw new Exception("Cannot unmarshal type Gender");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Gender)untypedValue;
            switch (value)
            {
                case Gender.Female:
                    serializer.Serialize(writer, "female");
                    return;
                case Gender.Male:
                    serializer.Serialize(writer, "male");
                    return;
            }
            throw new Exception("Cannot marshal type Gender");
        }

        public static readonly GenderConverter Singleton = new GenderConverter();
    }
}
