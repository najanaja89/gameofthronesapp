﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameOfThronesApp
{
    /// <summary>
    /// Interaction logic for CharacterInfo.xaml
    /// </summary>
    public partial class CharacterInfo : Window
    {
        public CharacterInfo(Models.GoTCharacters character)
        {
            InitializeComponent();
            if (character == null) { return; }
            if (character.Image == null)
            {
                infoImage.Source = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "/noPhoto.png", UriKind.RelativeOrAbsolute));
            }
            else infoImage.Source = new BitmapImage(character.Image);

            nameInfo.Text = character.Name;
            deathPlaceInfo.Text = $"Death place: {character.PlaceOfDeath?.ToString() ?? "Unknown"}";

            bornPlaceInfo.Text = $"Born place: {character.PlaceOfBirth?.ToString() ?? "Unknown"}";

            aliveInfo.Text = $"Is Alive: {character.Alive.ToString() ?? "Unknown"}";


            genderInfo.Text = $"Gender: {character.Gender?.ToString() ?? "Unknown"}";
            cultureInfo.Text = $"Culture: {character.Culture?.ToString() ?? "Unknown"}";
            fatherInfo.Text = $"Father: {character.Father?.ToString() ?? "Unknown"}";
            motherInfo.Text = $"Mother: {character.Mother?.ToString() ?? "Unknown"}";
            birthInfo.Text = $"Birth Date: {character.Birth?.ToString() ?? "Unknown"} AC";
            deathInfo.Text = $"Death Date: {character.Death?.ToString() ?? "Unknown"} AC";
            heirInfo.Text = $"Heir: {character.Heir?.ToString() ?? "Unknown"}";
            houseInfo.Text = $"House: {character.House?.ToString() ?? "Unknown"}";
            allegianceInfo.ItemsSource = character.Allegiance;
            childrenInfo.ItemsSource = character.Children;
            titleInfo.ItemsSource = character.Titles;
            spoucseInfo.ItemsSource = character.Spouse;
            bookInfo.ItemsSource = character.Books;

            bioInfo.Text = $"{character.Name} was born at { character.Birth?.ToString() ?? "Unknown"} AC in {character.PlaceOfBirth?.ToString() ?? "Unknown"} place in {character.House?.ToString() ?? "Unknown"} with {character.Culture?.ToString() ?? "Unknown"} culture. Father was {character.Father?.ToString() ?? "Unknown"} and Mother was {character.Mother?.ToString() ?? "Unknown"}. {character.Name} has heir {character.Heir?.ToString() ?? "Unknown"} and children: {string.Join(", ", character?.Children) ?? "Unknown"}. {character.Name} had titles: {string.Join(", ", character?.Titles) ?? "Unknown"} and kept allegiance to { string.Join(", ", character?.Allegiance) ?? "Unknown"}. {character.Name} dead at {character.Death?.ToString() ?? "Unknown"} AC in {character.PlaceOfDeath?.ToString() ?? "Unknown"} place. Appiried in books: { string.Join(", ", character?.Books) ?? "Unknown"}";


        }
    }
}
