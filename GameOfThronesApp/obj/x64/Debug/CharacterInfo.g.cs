﻿#pragma checksum "..\..\..\CharacterInfo.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "54412CA2A8A88216F2FCD713C7FFD3CB4C6F9121"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CefSharp.Wpf;
using GameOfThronesApp;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GameOfThronesApp {
    
    
    /// <summary>
    /// CharacterInfo
    /// </summary>
    public partial class CharacterInfo : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 19 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid characterInfoGrid;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock deathPlaceInfo;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock aliveInfo;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock bornPlaceInfo;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock nameInfo;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image infoImage;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock birthInfo;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock deathInfo;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock genderInfo;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock cultureInfo;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fatherInfo;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock motherInfo;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock heirInfo;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock houseInfo;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView spoucseInfo;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView allegianceInfo;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView childrenInfo;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView titleInfo;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView bookInfo;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\CharacterInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock bioInfo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GameOfThronesApp;component/characterinfo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\CharacterInfo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.characterInfoGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.deathPlaceInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.aliveInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.bornPlaceInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.nameInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.infoImage = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.birthInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.deathInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.genderInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.cultureInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.fatherInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.motherInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.heirInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.houseInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.spoucseInfo = ((System.Windows.Controls.ListView)(target));
            return;
            case 16:
            this.allegianceInfo = ((System.Windows.Controls.ListView)(target));
            return;
            case 17:
            this.childrenInfo = ((System.Windows.Controls.ListView)(target));
            return;
            case 18:
            this.titleInfo = ((System.Windows.Controls.ListView)(target));
            return;
            case 19:
            this.bookInfo = ((System.Windows.Controls.ListView)(target));
            return;
            case 20:
            this.bioInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

