﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GameOfThronesApp.Models;

namespace GameOfThronesApp
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var webClient = new WebClient();
            var jsonString = webClient.DownloadString("https://api.got.show/api/book/characters");
            var goTCharacters = GoTCharacters.FromJson(jsonString).OrderBy(name => name.Name);
            webClient.Dispose();
            listCharacters.ItemsSource = goTCharacters;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(listCharacters.ItemsSource);
            view.Filter = UserFilter;
        }

        private void ListCharactersSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var character = (GoTCharacters)listCharacters.SelectedItem;
            var characterInfo = new CharacterInfo(character);
            if (character == null) { return; }
            else { characterInfo.Show(); }

            //MessageBox.Show(character.Name);
        }

        private bool UserFilter(object item)
        {
            if (string.IsNullOrEmpty(filterCharacter.Text))
                return true;
            else
                return ((item as GoTCharacters).Name.IndexOf(filterCharacter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private void FilterCharacterTextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(listCharacters.ItemsSource).Refresh();
        }
    }
}
